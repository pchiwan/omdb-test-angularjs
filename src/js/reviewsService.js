/// <reference path="../bower_components/angular/angular.js" />
/// <reference path="js.extensions.js" />

angular.module('app.services')
	.service('reviewsService', function ($http, baseService) {
		
		var apiUrl = 'http://play.astrolab.io:4100/api/reviews';
			
		this.getByUserId = function (userId) {
			
			return baseService.get('{0}/userid/{1}'.format(apiUrl, userId))
				.then(function (data) {
					return !!data.data ? data.data : []
				});
		};

		this.getByImdbId = function (imdbId) {
		
			return baseService.get('{0}/imdbid/{1}'.format(apiUrl, imdbId))
				.then(function (data) {
					return !!data.data ? data.data : []
				});
		};		
	
		this.createReview = function (review) {
		
			var url = '{0}'.format(apiUrl);
			
			return $http({
				url: url,
				method: 'POST',
				data: review,
				headers: {
					'Content-Type': 'application/json'
 				}							
			}).then(function (data) {
				return !!data.data && !!data.data.result && !!data.data.result.ok
					? true
					: false;
			});
		};
	});