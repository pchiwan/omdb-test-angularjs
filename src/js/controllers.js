/// <reference path="../bower_components/angular/angular.js" />
/// <reference path="../bower_components/underscore/underscore.js" />

angular.module('app.controllers', ['app.services', 'ui.bootstrap'])
	.controller('appController', ['$scope', '$modal', '$timeout', '$injector',  
	
		function ($scope, $modal, $timeout, $injector) {
		
			var self = this,
				KEY_ENTER = 13,
				STATUS_OK = 200,
				modalInstance,
				omdbService = $injector.get('omdbService'),
				loginService = $injector.get('loginService'),
				reviewsService = $injector.get('reviewsService'),
				reviewModel = $injector.get('reviewModel'),
				userModel = $injector.get('userModel');
			
			this.searchText = '';
			this.results = [];
			this.orderBy = 'Year';
			this.orderDesc = false;
			this.isLoadingTop = false;
			this.isLoadingBottom = false;
			this.isLoadingUser = false;
			this.createNewUser = false;
			this.loginVisible = true;
			this.detail = null;
			this.user = new userModel();
			this.newReview = new reviewModel();
			
			Object.defineProperty(this, 'hasResults', {
				get: function () {
					return self.results.length > 0;
				}
			});
	
			Object.defineProperty(this, 'hasDetail', {
				get: function () {
					return self.detail != null;
				}
			});
	
			Object.defineProperty(this, 'detailHasPoster', {
				get: function () {
					return !!self.detail 
						&& !!self.detail.Poster;	
				}
			});

			Object.defineProperty(this, 'showLoginButton', {
				get: function () {
					return !self.createNewUser && !self.isLoadingUser && self.user._id === null;
				}
			});
	
			Object.defineProperty(this, 'showCreateUserButton', {
				get: function () {
					return self.createNewUser && !self.isLoadingUser && self.user._id === null;
				}
			});

			Object.defineProperty(this, 'currentImdbId', {
				get: function () {
					return self.hasDetail ? self.detail.imdbID : '';
				}
			});

			//#region public methods
	
			this.search = function () {
				
				if (!self.searchText) return;
	
				self.isLoadingTop = true;
				self.results = [];
				omdbService.search(self.searchText).then(function (data) {
					if (!!data && data.hasOwnProperty('data') && data.data.hasOwnProperty('Search')) {
						self.results = data.data.Search;
					}
					self.detail = null;
					self.isLoadingTop = false;
				});								
			};
			
			this.searchKeyup = function (event) {
	
				if (event.keyCode == KEY_ENTER) { // enter
					self.search();
				}
			};
	
			this.getById = function (id) {
				
				self.isLoadingBottom = true;
				omdbService.getById(id).then(function (data) {
					self.detail = data.data;
					self.detail.Poster = omdbService.getPosterUrl(id);
					self.detail.ImdbUrl = omdbService.getImdbUrl(id);
					self.getReviews(id);
				});
			};
			
			this.getReviews = function (id) {
					
				self.reviews = [];
				self.newReview = new reviewModel();
				
				reviewsService.getByImdbId(id).then(function (data) {
					self.reviews = data.map(function (d) { return new reviewModel(d) });
					self.isLoadingBottom = false;
				});
			};

			this.isSorted = function (property) {
				
				return self.orderBy == property;	
			};
			
			this.sort = function (orderBy) {
			
				if (orderBy == self.orderBy) {
					self.orderDesc = !self.orderDesc;
				} else {
					self.orderBy = orderBy;
					self.orderDesc = false;
				}
			};
			
			this.publishReview = function () {
		
				if (!!self.newReview.userReview) {
					
					reviewsService.createReview({
						imdbId: self.currentImdbId,
						score: self.newReview.score,
						userId: self.user._id,
						username: self.user.name,
						userReview: self.newReview.userReview,
						created: Date()
					}).then(function (result) {
						if (result) {
							self.newReview = new reviewModel();
							self.getReviews(self.currentImdbId);
						}
					});
				}
			};
			
			this.getUser = function () {
		
				if (!!self.user.username) {
					self.isLoadingUser = true;
					loginService.getUser(self.user.username)
						.then(function (user) {
							if (user) {
								completeLogin(user);
							} else {
								self.isLoadingUser = false;
								self.createNewUser = true;
							}
						});
				}
			};
		
			this.loginKeyup = function (event) {

				if (event.keyCode == KEY_ENTER) { // enter
					self.getUser();
				}		
			};

			this.createUser = function () {
			
				if (!!self.user.username && !!self.user.name) {
					self.isLoadingUser = true;
					loginService.createUser({
						username: self.user.username,
						name: self.user.name
					}).then(function (user) {
						if (!!user) {
							completeLogin(user);
						}
					});
				}
			};
			
			this.logout = function () {

				var d = new Date();
				d.setDate(d.getDate() - 1);
				document.cookie = "username=; expires={0}; path=/".format(d.toUTCString());

				self.loginVisible = true;

				modalInstance = $modal.open({
					animation: true,
					templateUrl: 'logoutModal.html',
					windowTemplateUrl: 'loginModalWindow.html',
					backdrop: 'static',
					keyboard: false,
					scope: $scope
				});		
			};

			this.reload = function () {

				location.reload();
			};

			//#endregion
			
			//#region Private methods
		
			var completeLogin = function (data) {
				$scope.$applyAsync(function () {
					self.user = data;
					if (self.loginVisible) {
						var d = new Date();
						d.setDate(d.getDate() + 1);					
						document.cookie = "username={0}; expires={1}; path=/".format(self.user.username, d.toUTCString());

						$timeout(function () {
							modalInstance.close();
							self.loginVisible = false;
						}, 2000);
					} else {
						angular.element('.container').removeClass('hidden');
					}
				});		
			};

			var init = function () {
								
				if (!!document.cookie && document.cookie.contains('username') >= 0) {
					var cookies = document.cookie.split(';');
					var cookie = _.find(cookies, function (x) { return x.contains('username'); });
					
					if (!!cookie) {
						var username = cookie.split('=')[1];
						username = username.replace('""', '');
						if (!!username) {
							self.user.username = username;
							self.loginVisible = false;
							self.getUser();
							return;
						}
					}
				}

				modalInstance = $modal.open({
					animation: true,
					templateUrl: 'loginModal.html',
					windowTemplateUrl: 'loginModalWindow.html',
					backdrop: 'static',
					keyboard: false,
					scope: $scope
				});
			};						
			
			//#endregion
			
			init();
		}]);