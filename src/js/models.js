
angular.module('app.models', [])
	.factory('reviewModel', function () {
		
		return function (data) {
	
			this._id = !!data && data.hasOwnProperty('_id') ? data._id : null;
			this.created = !!data && data.hasOwnProperty('created') ? new Date(data.created) : null;
			this.imdbId = !!data && data.hasOwnProperty('imdbId') ? data.imdbId : '';
			this.score = !!data && data.hasOwnProperty('score') ? data.score : 0;
			this.userId = !!data && data.hasOwnProperty('userId') ? data.userId : null;
			this.username = !!data && data.hasOwnProperty('username') ? data.username : '';
			this.userReview = !!data && data.hasOwnProperty('userReview') ? data.userReview : '';
		};
	})
	.factory('userModel', function () {
		
		return function (data) {

			this._id = !!data && data.hasOwnProperty('_id') ? data._id : null;
			this.name = !!data && data.hasOwnProperty('name') ? data.name : null;
			this.username = !!data && data.hasOwnProperty('username') ? data.username : '';
		};
	});
