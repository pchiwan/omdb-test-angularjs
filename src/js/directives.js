
angular.module('app.directives', [])
	.directive('score', function () {
		
		var link = function (scope, element, attr) {

			scope.scoreOptions = [];

			scope.scoreEquals = function (score) {
				return scope.ngModel >= score;
			};

			scope.changeScore = function (newScore) {
				if (!scope.disabled) {
					scope.ngModel = newScore;
				}
			};

			scope.hoverIn = function(e) {
				var $target = $(e.target);
				$target.prevAll().andSelf().addClass('hovered');
			};

			scope.hoverOut = function(e) {
				var $target = $(e.target);
				$target.prevAll().andSelf().removeClass('hovered');
			};

			for (var i = 0, l = 5; i < l; i++) {
				scope.scoreOptions.push({ id: '{0}-{1}'.format(scope.name, i + 1), value: i + 1 })	
			}
		};

		return {
			link: link,
			template: 
				'<div class="review-score" ng-class="{ disabled: disabled }">' +
					'<label ng-repeat="option in scoreOptions" ng-class="{ active: scoreEquals(option.value) }" ng-click="changeScore(option.value)"' +
						'	ng-mouseenter="hoverIn($event)" ng-mouseleave="hoverOut($event)"></label>' +
				'</div>',
			scope: {
				name: '@',
				ngModel: '=',
				disabled: '=?ngDisabled'
			},
			replace: true,
			restrict: 'AE'
		}
	})
	.directive('centerVertically', function ($timeout) {

		var link = function (scope, element, attr) {

			$timeout(function () {
				var content = element.find('.modal-content'),
					top = 0;
				content.css('display', 'block');
				top = Math.round(($(window).height() - content.height()) / 2);
			    content.css('margin-top', top > 0 ? top : 0);
			}, 0);
		};

		return {
			link: link,
			restrict: 'A'
		};
	});