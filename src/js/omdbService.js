/// <reference path="../bower_components/angular/angular.js" />
/// <reference path="js.extensions.js" />

angular.module('app.services')
	.service('omdbService', function ($http, baseService) {
		
		var apiUrl = 'http://www.omdbapi.com/?r=json&apikey={0}&',
			imgApiUrl = 'http://img.omdbapi.com/?i={0}&apikey={1}',
			imdbUrl = 'http://imdb.com/title/{0}/',
			APIKEY = 'e581c30d'; // this is MY API key, I paid for it!		

		this.search = function (searchText) {
			
			return baseService.get('{0}s={1}'.format(apiUrl.format(APIKEY), searchText));			
		};
		
		this.getByTitle = function (title) {
			
			return baseService.get('{0}t={1}'.format(apiUrl.format(APIKEY), title));
		};
		
		this.getById = function (imdbid) {
			
			return baseService.get('{0}i={1}'.format(apiUrl.format(APIKEY), imdbid));
		};

		this.getPosterUrl = function (imdbid) {
			return imgApiUrl.format(imdbid, APIKEY);
		};

		this.getImdbUrl = function (imdbid) {
			return imdbUrl.format(imdbid);
		};
	});
		