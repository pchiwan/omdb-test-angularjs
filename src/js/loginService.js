/// <reference path="../bower_components/angular/angular.js" />
/// <reference path="js.extensions.js" />

angular.module('app.services')
	.service('loginService', function ($http, baseService) {

		var apiUrl = 'http://play.astrolab.io:4100/api/users';

		this.getUser = function (username) {
			
			return baseService.get('{0}/username/{1}'.format(apiUrl, username))
				.then(function (data) {
					return !$.isEmptyObject(data.data) ? data.data : null;
				});
		};
	
		this.createUser = function (user) {
			
			var url = '{0}'.format(apiUrl);
			
			return $http({
				url: url,
				method: 'POST',
				data: user,
				headers: {
					'Content-Type': 'application/json'
 				}
			}).then(function (data) {
				return !!data.data && !!data.data.ops && data.data.ops.length
					? data.data.ops[0]				
					: null;
			});
		};
	});