/// <reference path="../bower_components/angular/angular.js" />

angular.module('app.services', [])
	.service('baseService', function ($http) {
		
		this.get = function (url) {
			
			return $http({
				url: url,
				method: 'GET'
			})	
			.error(function (data) {
				console.log(data);
			});
		};
	});