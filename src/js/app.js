/// <reference path="../bower_components/angular/angular.js" />

// application
angular.module('app', ['app.controllers', 'app.directives', 'app.models']);